# CakeX

An opinionated fork of CakePHP 1.3.X based on <cakephp1.3.x-php7> with focus on built-in features for:
- api versioning
- api scaffolding
- api tests / coverage generation
- reactJs integration
- deployment
- translation
- phinx



# Installation
###########################

# Configuration
###########################

# API versioning support
###########################

## Routes setup
- added config/routes dir
- added routes_helper.php
- updated config/routes.php to include routes_helper

- requireApiRoutes();
- added base classes in controllers
api_controller.php
apiv2_controller.php

## Directory structure
- controller/api/apiv2_controller.php
- controller/api/v2/

## Tests for API
- TODO


# ReactJS Integration
###########################

- add react elements to views/elements/react/ <-- move to views/react
- react views/react_components/ has a directory structure
- each components has:
-- react_css.ctp
-- react_js.ctp
-- react_view.ctp

-  In the separate react app <-- script to inject / integrate
