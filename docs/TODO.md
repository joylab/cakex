# CakeX 
PHP7 / reactjs / composer / environments / translations

TODO create samples / or add as defaults to the cakephp 1.3 repo

## Check environment dependencies
e.g. php7+

## Steps to get it working so far inside app directory
(DONE) - add config/core.php
(DONE) - add config/database.php
(DONE) - add config/.env.php
(DONE) - add config/environment.php
(DONE) - add config/bootstrap.php
(DONE) - add composer.json
(DONE) - add composer.phar
(DONE) - add default views/pages/welcome.ctp
(DONE) - update config/routes default to go to pages/welcome
- Setup tmp folder: 

mkdir -p tmp/cache/models/ tmp/cache/persistent; chmod -R 777 tmp


## Steps the setup script could ask for
- project name
- admin emails
- timezone
- environments
- database to use
- generates new seed
- base domain name
- languages


## TODO

### Setup script 
Goal:
One CLI script (with API) to manage all things related to Project
- configurator
- code generator
- bake: API creator / scaffolds (repurpose or wrap)
- wrapper for migrations / releases
- extendable

1. standalone setup script that will:
-- read-in inputs from user based on questions list
-- can you give options? e.g. e.g. user presses 1. Asia/Beirut, 2. America/New_York
-- library fro CLI colors <--- 

2. read files from sample directory
3. generate files based on inputs (find strings and replace in files)
e.g. in sample file find %%COOKIE_NAME_HERE%% <-- str_replace_all and place
in appropriate folder

#### ReactJS support / placeholder (generator)
#### API versioning (generator)
#### API Documentation
#### Phpunit + tests
#### Phinx

### Phase 2
#### Sentry (optional via CLI)
#### Papertrail (optional via CLI)
#### GA (optional via CLI)
#### Translations (optional via CLI)
#### Cron jobs handling


