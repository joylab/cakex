<?php
class DATABASE_CONFIG {

	public $default = array(
		'driver' => 'mysqli',
		'persistent' => false,
		'host' => 'localhost',
		'login' => DATABASE_LOGIN,
		'password' => DATABASE_PASSWORD,
		'database' => DATABASE_NAME,
		'prefix' => '',
		'encoding' => 'UTF8',
	);

	public $test = array(
		'driver' => 'mysqli',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'test_database_name',
		'prefix' => '',
		'encoding' => 'UTF8',
	);
}
