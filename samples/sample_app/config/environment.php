<?php
// path to CakePHP (usually bundled with the application)
if (!defined('CAKE_CORE_INCLUDE_PATH')) {
define('CAKE_CORE_INCLUDE_PATH', ROOT . '/' . APP_DIR . '/cake/CakePHP');
}

/**
 * Detect current environment is dev|staging|production
 *
 * Checks for convention in APP_DIR of curent app, defaulting to null
 *
 * @author Joy Khoriaty
 * @return string String indicating 'dev'|'staging'|'production environment
 */
function identifyEnvironment() {
	$subject = ROOT . '/' . APP_DIR;
	if (preg_match('/production/i', $subject)) {
		return 'production';
	}
	elseif (preg_match('/staging/i', $subject)) {
		return 'staging';
	}
	elseif (preg_match('/dev/i', $subject)) {
		return 'dev';
	}
	else
		return null;
}
