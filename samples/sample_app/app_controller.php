<?php
/**
 *
 */
class AppController extends Controller {
	/**
	 *
	 */
	public $components = [
		#'P28n',
		#'Auth',
		'RequestHandler',
		'Session'
	];

	/**
	 *
	 */
	public $helpers = [
		'Html',
		'Form',
		'Time',
		'Session',
		'Javascript'
	];

	/**
	 *
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 *
	 */
	public function beforeRender() {
		## Set Appropriate layout
		if ($this->Session->check('Auth.User.id')) {
			$this->layout = 'loggedin';
		}

	  ## Set active language view variable
    #$active_language = $this->Session->read('Config.language');
    #$this->set('active_language', $active_language);
    #$this->setTranslation(Configure::read('TRANSLATION_DEFAULT_NAMESPACE'), $active_language);
	}

	/**
	 *
	 */
	public function beforeFilter() {

		# Define Auth Allowed actions
		############################################
		# $this->Auth->allow('*');
    /*
		$this->Auth->allow(
			'*',
			## Language related
			'change',

			## Public landing page
			'index',

			'login'
		);
    */

		# Set active language view variable
		############################################
    #$active_language = $this->Session->read('Config.language');
    #$this->set('active_language', $active_language);
	}


}
